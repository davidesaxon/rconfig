use std::collections::HashMap;

// -----------------------------------------------------------------------------
//                                    N O D E
// -----------------------------------------------------------------------------

// -----------------------------------E N U M-----------------------------------
/// Node enum object which represents typed data for RConfig
#[derive(Clone, Debug, PartialEq, serde::Serialize, serde::Deserialize)]
#[serde(untagged)]
pub enum Node {
    /// `Group` variant of `Node` which holds named child `Node`s.
    Group(HashMap<String, Node>),

    /// `List` variant of `Node` which holds a vector of Nodes.
    List(Vec<Node>),

    /// `Bool` variant of `Node` which holds a boolean value.
    Bool(bool),

    /// `Int` variant of `Node` which holds an i64 integer.
    Int(i64),

    /// `Float` variant of `Node` which holds a f64 floating point number.
    Float(f64),

    /// `String` variant of `Node` which holds a String.
    String(String),
}

// -----------------------------------------------------------------------------
//                                   T E S T S
// -----------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_serialize() {
        let node = Node::Group(HashMap::new());
        let generated = serde_json::to_value(&node).unwrap();
        let expected = serde_json::json!({});
        assert_eq!(generated, expected);

        let mut group = HashMap::new();
        group.insert("key".to_string(), Node::Bool(true));
        let node = Node::Group(group);
        let generated = serde_json::to_value(&node).unwrap();
        let expected = serde_json::json!({"key": true});
        assert_eq!(generated, expected);

        let mut group = HashMap::new();
        group.insert("a".to_string(), Node::Int(14));
        group.insert("b".to_string(), Node::String("Hello World".to_string()));
        let mut sub_group = HashMap::new();
        sub_group.insert("child1".to_string(), Node::Float(3.14));
        sub_group.insert(
            "child2".to_string(),
            Node::List(vec![Node::Bool(true), Node::Bool(false)]),
        );
        group.insert("c".to_string(), Node::Group(sub_group));
        let node = Node::Group(group);
        let generated = serde_json::to_value(&node).unwrap();
        let expected = serde_json::json!({
            "a": 14,
            "b": "Hello World",
            "c": {
                "child1": 3.14,
                "child2": [true, false]
            }
        });
        assert_eq!(generated, expected);

        let node = Node::List(vec![Node::Bool(true), Node::Bool(false)]);
        let generated = serde_json::to_value(&node).unwrap();
        let expected = serde_json::json!([true, false]);
        assert_eq!(generated, expected);

        let node = Node::Bool(true);
        let generated = serde_json::to_value(&node).unwrap();
        let expected = serde_json::json!(true);
        assert_eq!(generated, expected);

        let node = Node::Int(1);
        let generated = serde_json::to_value(&node).unwrap();
        let expected = serde_json::json!(1);
        assert_eq!(generated, expected);

        let node = Node::Float(3.14);
        let generated = serde_json::to_value(&node).unwrap();
        let expected = serde_json::json!(3.14);
        assert_eq!(generated, expected);

        let node = Node::String("Hello world!".to_string());
        let generated = serde_json::to_value(&node).unwrap();
        let expected = serde_json::json!("Hello world!");
        assert_eq!(generated, expected);
    }

    #[test]
    fn test_deserialize() {
        let json = "{}";
        let node: Node = serde_json::from_str(json).unwrap();
        match node {
            Node::Group(map) => assert!(map.is_empty()),
            _ => panic!("Deserialized node is not a group"),
        }

        let json = r#"{"a": true, "b": "Hello world!"}"#;
        let node: Node = serde_json::from_str(json).unwrap();
        match node {
            Node::Group(map) => {
                if let Node::Bool(b) = map.get("a").unwrap() {
                    assert_eq!(*b, true);
                } else {
                    panic!("Child \"a\" is not a bool");
                }
                if let Node::String(s) = map.get("b").unwrap() {
                    assert_eq!(*s, "Hello world!".to_string());
                } else {
                    panic!("Child \"b\" is not a string");
                }
            }
            _ => panic!("Deserialized node is not a group"),
        }

        let json = r#"["item1", "item2"]"#;
        let node: Node = serde_json::from_str(json).unwrap();
        match node {
            Node::List(vec) => {
                assert_eq!(vec.len(), 2);
                if let Node::String(s) = &vec[0] {
                    assert_eq!(*s, "item1".to_string());
                } else {
                    panic!("Child \"item1\" is not a string");
                }
                if let Node::String(s) = &vec[1] {
                    assert_eq!(*s, "item2".to_string());
                } else {
                    panic!("Child \"item2\" is not a string");
                }
            }
            _ => panic!("Deserialized node is not a list"),
        }

        let json = "true";
        let node: Node = serde_json::from_str(json).unwrap();
        match node {
            Node::Bool(b) => assert_eq!(b, true),
            _ => panic!("Deserialized node is not a bool"),
        }

        let json = "44";
        let node: Node = serde_json::from_str(json).unwrap();
        match node {
            Node::Int(i) => assert_eq!(i, 44),
            _ => panic!("Deserialized node is not an int"),
        }

        let json = "3.14";
        let node: Node = serde_json::from_str(json).unwrap();
        match node {
            Node::Float(f) => assert_eq!(f, 3.14),
            _ => panic!("Deserialized node is not a float"),
        }

        let json = r#""Hello world!""#;
        let node: Node = serde_json::from_str(json).unwrap();
        match node {
            Node::String(s) => assert_eq!(s, "Hello world!"),
            _ => panic!("Deserialized node is not a string"),
        }
    }
}
