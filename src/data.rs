use std::collections::HashMap;
use std::fs::File;
use std::io::BufReader;
use std::path::Path;
use std::sync::RwLock;

use crate::node::Node;

// -----------------------------------------------------------------------------
//                                    D A T A
// -----------------------------------------------------------------------------

// ---------------------------------S T R U C T---------------------------------
/// Object used to store and access RConfig configuration data which is thread
/// safe.
pub struct Data {
    root: RwLock<Node>,
}

// -------------------------I M P L E M E N T A T I O N-------------------------
impl Data {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Creates new empty RConfig `Data`
    pub fn new() -> Data {
        Data {
            root: RwLock::new(Node::Group(HashMap::new())),
        }
    }

    /// Creates new RConfig `Data` by loading it from the given file.
    pub fn load(path: &Path) -> Result<Data, String> {
        // check whether the file exists
        if !path.exists() {
            return Err(format!("File does not exist: {}", path.display()));
        }

        // load the file
        let file = match File::open(&path) {
            Ok(v) => v,
            Err(e) => {
                return Err(format!("Could not read file: {}", e.to_string()));
            }
        };
        let reader = BufReader::new(file);

        // parse as json
        let json: serde_json::Value = match serde_json::from_reader(reader) {
            Ok(v) => v,
            Err(e) => {
                return Err(format!(
                    "Could not parse json from file {} with error: {}",
                    path.display(),
                    e.to_string()
                ));
            }
        };

        let root = match serde_json::from_value(json) {
            Ok(v) => v,
            Err(e) => {
                return Err(format!(
                    "Could not parse json from file {} with error: {}",
                    path.display(),
                    e.to_string()
                ));
            }
        };

        return Ok(Data {
            root: RwLock::new(root),
        });
    }

    // -----------------------------PUBLIC FUNCTIONS----------------------------
    /// Returns the data stored at the given path.
    pub fn get_node(&self, path: &str) -> Option<Node> {
        let r_root = self.root.read().unwrap();
        let mut node = &*r_root;
        let mut path = path.split(".").peekable();

        while let Some(key) = path.next() {
            match node {
                Node::Group(map) => {
                    if let Some(next) = map.get(key) {
                        node = &next;
                    } else {
                        return None;
                    }
                }
                _ => return None,
            }
        }

        return Some(node.clone());
    }

    /// Sets the value of the data node stored at the given path
    /// Note: The path must exist in this configuration `Data` and the given
    ///       `Node` value must match the type of the existing value.
    ///
    /// \returns The previous value stored at the given path
    pub fn set_node(
        &mut self,
        path: &str,
        value: Node,
    ) -> Result<Node, String> {
        let mut w_root = self.root.write().unwrap();
        let mut node = &mut *w_root;
        let mut path_components = path.split(".").peekable();

        while let Some(key) = path_components.next() {
            match node {
                Node::Group(map) => {
                    // final key?
                    if path_components.peek().is_none() && map.contains_key(key)
                    {
                        // do the types match?
                        if std::mem::discriminant(map.get(key).unwrap())
                            != std::mem::discriminant(&value)
                        {
                            return Err(format!(
                                "Cannot set config value at path {} with \
                                different type",
                                path
                            ));
                        }
                        // set value
                        return Ok(map.insert(key.to_string(), value).unwrap())
                            .clone();
                    }
                    // otherwise continue recursing
                    if let Some(next) = map.get_mut(key) {
                        if path_components.peek().is_none() {
                            break;
                        }
                        node = next;
                    } else {
                        return Err(format!(
                            "Cannot set new config key at non-existing path: \
                            {}",
                            path
                        ));
                    }
                }
                _ => {
                    return Err(format!(
                        "Cannot set new config key at non-existing path: {}",
                        path
                    ))
                }
            }
        }

        return Err(format!(
            "Cannot set new config key at non-existing path: {}",
            path
        ));
    }

    /// Returns the list of values stored at the given path
    /// If the path doesn't exist or the value is not a List, returns None
    pub fn get_list(&self, path: &str) -> Option<Vec<Node>> {
        match self.get_node(path) {
            Some(Node::List(vec)) => Some(vec.clone()),
            _ => None,
        }
    }

    /// Sets the list of values stored at the given path
    /// If the path doesn't exist or the value is not a List, returns an error
    pub fn set_list(
        &mut self,
        path: &str,
        value: Vec<Node>,
    ) -> Result<Vec<Node>, String> {
        if let Node::List(l) = self.set_node(path, Node::List(value))? {
            return Ok(l);
        }
        // should be unreachable
        panic!("Previous value is not a list");
    }

    /// Returns the bool value stored at the given path
    /// If the path doesn't exist or the value is not a Bool, returns None
    pub fn get_bool(&self, path: &str) -> Option<bool> {
        match self.get_node(path) {
            Some(Node::Bool(b)) => Some(b),
            _ => None,
        }
    }

    /// Sets the bool value stored at the given path
    /// If the path doesn't exist or the value is not a Bool, returns an error
    pub fn set_bool(
        &mut self,
        path: &str,
        value: bool,
    ) -> Result<bool, String> {
        if let Node::Bool(b) = self.set_node(path, Node::Bool(value))? {
            return Ok(b);
        }
        // should be unreachable
        panic!("Previous value is not a bool");
    }

    /// Returns the i32 value stored at the given path
    /// If the path doesn't exist or the value is not an Int, returns None
    pub fn get_i32(&self, path: &str) -> Option<i32> {
        match self.get_node(path) {
            Some(Node::Int(i)) => Some(i as i32),
            _ => None,
        }
    }

    /// Sets the i32 value stored at the given path
    /// If the path doesn't exist or the value is not an Int, returns an error
    pub fn set_i32(&mut self, path: &str, value: i32) -> Result<i32, String> {
        if let Node::Int(i) = self.set_node(path, Node::Int(value as i64))? {
            return Ok(i as i32);
        }
        // should be unreachable
        panic!("Previous value is not an int");
    }

    /// Returns the u32 value stored at the given path
    /// If the path doesn't exist or the value is not an Int, returns None
    pub fn get_u32(&self, path: &str) -> Option<u32> {
        match self.get_node(path) {
            Some(Node::Int(i)) => Some(i as u32),
            _ => None,
        }
    }

    /// Sets the u32 value stored at the given path
    /// If the path doesn't exist or the value is not an Int, returns an error
    pub fn set_u32(&mut self, path: &str, value: u32) -> Result<u32, String> {
        if let Node::Int(i) = self.set_node(path, Node::Int(value as i64))? {
            return Ok(i as u32);
        }
        // should be unreachable
        panic!("Previous value is not an int");
    }

    /// Returns the i64 value stored at the given path
    /// If the path doesn't exist or the value is not an Int, returns None
    pub fn get_i64(&self, path: &str) -> Option<i64> {
        match self.get_node(path) {
            Some(Node::Int(i)) => Some(i as i64),
            _ => None,
        }
    }

    /// Sets the i64 value stored at the given path
    /// If the path doesn't exist or the value is not an Int, returns an error
    pub fn set_i64(&mut self, path: &str, value: i64) -> Result<i64, String> {
        if let Node::Int(i) = self.set_node(path, Node::Int(value as i64))? {
            return Ok(i as i64);
        }
        // should be unreachable
        panic!("Previous value is not an int");
    }

    /// Returns the u64 value stored at the given path
    /// If the path doesn't exist or the value is not an Int, returns None
    pub fn get_u64(&self, path: &str) -> Option<u64> {
        match self.get_node(path) {
            Some(Node::Int(i)) => Some(i as u64),
            _ => None,
        }
    }

    /// Sets the u64 value stored at the given path
    /// If the path doesn't exist or the value is not an Int, returns an error
    pub fn set_u64(&mut self, path: &str, value: u64) -> Result<u64, String> {
        if let Node::Int(i) = self.set_node(path, Node::Int(value as i64))? {
            return Ok(i as u64);
        }
        // should be unreachable
        panic!("Previous value is not an int");
    }

    /// Returns the usize value stored at the given path
    /// If the path doesn't exist or the value is not an Int, returns None
    pub fn get_usize(&self, path: &str) -> Option<usize> {
        match self.get_node(path) {
            Some(Node::Int(i)) => Some(i as usize),
            _ => None,
        }
    }

    /// Sets the usize value stored at the given path
    /// If the path doesn't exist or the value is not an Int, returns an error
    pub fn set_usize(
        &mut self,
        path: &str,
        value: usize,
    ) -> Result<usize, String> {
        if let Node::Int(i) = self.set_node(path, Node::Int(value as i64))? {
            return Ok(i as usize);
        }
        // should be unreachable
        panic!("Previous value is not an int");
    }

    /// Returns the f32 value stored at the given path
    /// If the path doesn't exist or the value is not a Float, returns None
    pub fn get_f32(&self, path: &str) -> Option<f32> {
        match self.get_node(path) {
            Some(Node::Float(f)) => Some(f as f32),
            _ => None,
        }
    }

    /// Sets the f32 value stored at the given path
    /// If the path doesn't exist or the value is not a Float, returns an error
    pub fn set_f32(&mut self, path: &str, value: f32) -> Result<f32, String> {
        if let Node::Float(f) =
            self.set_node(path, Node::Float(value as f64))?
        {
            return Ok(f as f32);
        }
        // should be unreachable
        panic!("Previous value is not a float");
    }

    /// Returns the f64 value stored at the given path
    /// If the path doesn't exist or the value is not a Float, returns None
    pub fn get_f64(&self, path: &str) -> Option<f64> {
        match self.get_node(path) {
            Some(Node::Float(f)) => Some(f as f64),
            _ => None,
        }
    }

    /// Sets the f64 value stored at the given path
    /// If the path doesn't exist or the value is not a Float, returns an error
    pub fn set_f64(&mut self, path: &str, value: f64) -> Result<f64, String> {
        if let Node::Float(f) = self.set_node(path, Node::Float(value))? {
            return Ok(f);
        }
        // should be unreachable
        panic!("Previous value is not a float");
    }

    /// Returns the string value stored at the given path
    /// If the path doesn't exist or the value is not a String, returns None
    pub fn get_string(&self, path: &str) -> Option<String> {
        match self.get_node(path) {
            Some(Node::String(s)) => Some(s.clone()),
            _ => None,
        }
    }

    /// Sets the string value stored at the given path
    /// If the path doesn't exist or the value is not a String, returns an error
    pub fn set_string(
        &mut self,
        path: &str,
        value: String,
    ) -> Result<String, String> {
        if let Node::String(s) = self.set_node(path, Node::String(value))? {
            return Ok(s);
        }
        // should be unreachable
        panic!("Previous value is not a string");
    }

    /// Applies the given configuation data's values to this data.
    ///
    /// Paths in the given data that don't exist in this data are ignore and
    /// likewise values in the given data that don't match the type of the
    /// values in this data are ignored.
    pub fn apply(&mut self, data: &Data) {
        self.overload_node("", &*data.root.read().unwrap());
    }

    // ----------------------------PRIVATE FUNCTIONS----------------------------
    fn overload_node(&mut self, parent_path: &str, node: &Node) {
        match node {
            // for groups, recursive into child nodes
            Node::Group(map) => {
                for (key, value) in map {
                    let mut path = key.clone();
                    if parent_path != "" {
                        path = format!("{}.{}", parent_path, path);
                    }
                    self.overload_node(&path, value);
                }
            }
            _ => {
                // for non-groups, set the node and ignore errors
                let _ = self.set_node(parent_path, node.clone());
            }
        }
    }
}

// -----------------------------------------------------------------------------
//                    C L O N E   I M P L E M E N T A T I O N
// -----------------------------------------------------------------------------

impl Clone for Data {
    fn clone(&self) -> Self {
        Data {
            root: RwLock::new(self.root.read().unwrap().clone()),
        }
    }
}

// -----------------------------------------------------------------------------
//          S E R D E   S E R I A L I Z E   I M P L E M E N T A T I O N
// -----------------------------------------------------------------------------

impl serde::Serialize for Data {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        self.root.read().unwrap().serialize(serializer)
    }
}

// -----------------------------------------------------------------------------
//        S E R D E   D E S E R I A L I Z E   I M P L E M E N T A T I O N
// -----------------------------------------------------------------------------

impl<'de> serde::Deserialize<'de> for Data {
    fn deserialize<D: serde::Deserializer<'de>>(
        deserializer: D,
    ) -> Result<Self, D::Error> {
        let node = Node::deserialize(deserializer)?;
        Ok(Data {
            root: RwLock::new(node),
        })
    }
}

// -----------------------------------------------------------------------------
//                                   T E S T S
// -----------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new() {
        let data = Data::new();
        let root = data.root.read().unwrap();

        match &*root {
            Node::Group(map) => assert!(map.is_empty()),
            _ => panic!("Root is not a group"),
        }
    }

    #[test]
    fn test_load() {
        let data = Data::load(Path::new("resources/tests/test_config_a.json"))
            .unwrap();

        assert_eq!(data.get_i64("group.int").unwrap(), 14);
        assert_eq!(data.get_string("group.string").unwrap(), "Hello world!");
        let int_list = data.get_list("int_list").unwrap();
        assert_eq!(int_list.len(), 4);
        assert_eq!(int_list[0], Node::Int(1));
        assert_eq!(int_list[1], Node::Int(2));
        assert_eq!(int_list[2], Node::Int(3));
        assert_eq!(int_list[3], Node::Int(4));
        let mixed_list = data.get_list("mixed_list").unwrap();
        assert_eq!(mixed_list.len(), 3);
        assert_eq!(mixed_list[0], Node::String("Hello".to_string()));
        assert_eq!(mixed_list[1], Node::Bool(true));
        assert_eq!(mixed_list[2], Node::Float(3.14));
        assert_eq!(data.get_bool("bool").unwrap(), true);
        assert_eq!(data.get_i64("int").unwrap(), 42);
        assert_eq!(data.get_f32("float").unwrap(), 3.14);
        assert_eq!(data.get_string("string").unwrap(), "test_test");
    }

    #[test]
    fn test_get_node() {
        let mut group = HashMap::new();
        group.insert("a".to_string(), Node::Int(14));
        group.insert("b".to_string(), Node::String("Hello world!".to_string()));
        let mut sub_group = HashMap::new();
        sub_group.insert("d".to_string(), Node::Float(3.14));
        group.insert("c".to_string(), Node::Group(sub_group));
        group.insert("e".to_string(), Node::Bool(true));

        let root = Node::Group(group);
        let data = Data {
            root: RwLock::new(root),
        };

        // get node
        let node_a = data.get_node("a");
        assert!(node_a.is_some());
        assert_eq!(node_a.unwrap(), Node::Int(14));

        let node_b = data.get_node("b");
        assert!(node_b.is_some());
        assert_eq!(node_b.unwrap(), Node::String("Hello world!".to_string()));

        let node_d = data.get_node("c.d");
        assert!(node_d.is_some());
        assert_eq!(node_d.unwrap(), Node::Float(3.14));

        // get node doesn't exist
        let node = data.get_node("does_not_exist");
        assert!(node.is_none());

        let node = data.get_node("d");
        assert!(node.is_none());

        let node = data.get_node("c.a");
        assert!(node.is_none());

        // get bool
        assert_eq!(data.get_bool("e").unwrap(), true);
        assert!(data.get_bool("a").is_none());
        assert!(data.get_bool("does_not_exist").is_none());

        // get i32
        assert_eq!(data.get_i32("a").unwrap(), 14);
        assert!(data.get_i32("e").is_none());
        assert!(data.get_i32("does_not_exist").is_none());

        // get u32
        assert_eq!(data.get_u32("a").unwrap(), 14);
        assert!(data.get_u32("e").is_none());
        assert!(data.get_u32("does_not_exist").is_none());

        // get i64
        assert_eq!(data.get_i64("a").unwrap(), 14);
        assert!(data.get_i64("e").is_none());
        assert!(data.get_i64("does_not_exist").is_none());

        // get u64
        assert_eq!(data.get_u64("a").unwrap(), 14);
        assert!(data.get_u64("e").is_none());
        assert!(data.get_u64("does_not_exist").is_none());

        // get usize
        assert_eq!(data.get_usize("a").unwrap(), 14);
        assert!(data.get_usize("e").is_none());
        assert!(data.get_usize("does_not_exist").is_none());

        // get f32
        assert_eq!(data.get_f32("c.d").unwrap(), 3.14);
        assert!(data.get_f32("a").is_none());
        assert!(data.get_f32("does_not_exist").is_none());

        // get f64
        assert_eq!(data.get_f64("c.d").unwrap(), 3.14);
        assert!(data.get_f64("a").is_none());
        assert!(data.get_f64("does_not_exist").is_none());

        // get string
        assert_eq!(data.get_string("b").unwrap(), "Hello world!");
        assert!(data.get_string("c.d").is_none());
        assert!(data.get_string("does_not_exist").is_none());
    }

    #[test]
    fn test_set_node() {
        let mut data =
            Data::load(Path::new("resources/tests/test_config_a.json"))
                .unwrap();

        // test setting int
        assert_eq!(data.get_i64("int").unwrap(), 42);
        let previous = data.set_node("int", Node::Int(14)).unwrap();
        assert_eq!(previous, Node::Int(42));
        assert_eq!(data.get_i64("int").unwrap(), 14);

        // test setting list
        let previous = data
            .set_node(
                "int_list",
                Node::List(vec![Node::Int(5), Node::Int(6), Node::Int(7)]),
            )
            .unwrap();
        if let Node::List(vec) = previous {
            assert_eq!(vec.len(), 4);
            assert_eq!(vec[0], Node::Int(1));
            assert_eq!(vec[1], Node::Int(2));
            assert_eq!(vec[2], Node::Int(3));
            assert_eq!(vec[3], Node::Int(4));
        } else {
            panic!("Previous value is not a list");
        }
        let int_list = data.get_list("int_list").unwrap();
        assert_eq!(int_list.len(), 3);
        assert_eq!(int_list[0], Node::Int(5));
        assert_eq!(int_list[1], Node::Int(6));
        assert_eq!(int_list[2], Node::Int(7));

        // test setting nested
        let previous = data
            .set_node(
                "group.string",
                Node::String("Goodbye world!".to_string()),
            )
            .unwrap();
        assert_eq!(previous, Node::String("Hello world!".to_string()));
        assert_eq!(data.get_string("group.string").unwrap(), "Goodbye world!");

        // test setting non-existing path
        let result = data.set_node("does_not_exist", Node::Int(14));
        assert!(result.is_err());

        // test setting wrong type
        let result = data.set_node("group.string", Node::Int(14));
        assert!(result.is_err());
    }

    #[test]
    fn test_typed_setters() {
        let mut data =
            Data::load(Path::new("resources/tests/test_config_a.json"))
                .unwrap();

        // test set_list
        let test_list = vec![Node::Int(8), Node::Int(9)];
        let previous = data.set_list("int_list", test_list.clone()).unwrap();
        assert_eq!(previous.len(), 4);
        assert_eq!(previous[0], Node::Int(1));
        assert_eq!(previous[1], Node::Int(2));
        assert_eq!(previous[2], Node::Int(3));
        assert_eq!(previous[3], Node::Int(4));
        let int_list = data.get_list("int_list").unwrap();
        assert_eq!(int_list.len(), 2);
        assert_eq!(int_list[0], Node::Int(8));
        assert_eq!(int_list[1], Node::Int(9));
        let result = data.set_list("does_not_exist", test_list.clone());
        assert!(result.is_err());
        let result = data.set_list("group.string", test_list.clone());
        assert!(result.is_err());

        // test set_bool
        let previous = data.set_bool("bool", false).unwrap();
        assert_eq!(previous, true);
        assert_eq!(data.get_bool("bool").unwrap(), false);
        let result = data.set_bool("does_not_exist", false);
        assert!(result.is_err());
        let result = data.set_bool("group.string", false);
        assert!(result.is_err());

        // test set_i32
        let previous = data.set_i32("int", 14).unwrap();
        assert_eq!(previous, 42);
        assert_eq!(data.get_i32("int").unwrap(), 14);
        let result = data.set_i32("does_not_exist", 14);
        assert!(result.is_err());
        let result = data.set_i32("group.string", 14);
        assert!(result.is_err());

        // test set_u32
        let previous = data.set_u32("int", 33).unwrap();
        assert_eq!(previous, 14);
        assert_eq!(data.get_u32("int").unwrap(), 33);
        let result = data.set_u32("does_not_exist", 33);
        assert!(result.is_err());
        let result = data.set_u32("group.string", 33);
        assert!(result.is_err());

        // test set_i64
        let previous = data.set_i64("int", 66).unwrap();
        assert_eq!(previous, 33);
        assert_eq!(data.get_i64("int").unwrap(), 66);
        let result = data.set_i64("does_not_exist", 66);
        assert!(result.is_err());
        let result = data.set_i64("group.string", 66);
        assert!(result.is_err());

        // test set_u64
        let previous = data.set_u64("group.int", 42).unwrap();
        assert_eq!(previous, 14);
        assert_eq!(data.get_u64("group.int").unwrap(), 42);
        let result = data.set_u64("group.does_not_exist", 42);
        assert!(result.is_err());
        let result = data.set_u64("string", 42);
        assert!(result.is_err());

        // test set_usize
        let previous = data.set_usize("group.int", 75).unwrap();
        assert_eq!(previous, 42);
        assert_eq!(data.get_usize("group.int").unwrap(), 75);
        let result = data.set_usize("group.does_not_exist", 75);
        assert!(result.is_err());
        let result = data.set_usize("string", 75);
        assert!(result.is_err());

        // test set_f32
        let previous = data.set_f32("float", 0.1).unwrap();
        assert_eq!(previous, 3.14);
        assert_eq!(data.get_f32("float").unwrap(), 0.1);
        let result = data.set_f32("group.does_not_exist", 0.1);
        assert!(result.is_err());
        let result = data.set_f32("group.string", 0.1);
        assert!(result.is_err());

        // test set_f64
        let previous = data.set_f64("float", 1.5).unwrap();
        assert!((previous - 0.1).abs() < f32::EPSILON as f64);
        assert_eq!(data.get_f64("float").unwrap(), 1.5);
        let result = data.set_f64("group.does_not_exist", 1.5);
        assert!(result.is_err());
        let result = data.set_f64("string", 1.5);
        assert!(result.is_err());

        // test set_string
        let previous = data
            .set_string("group.string", "Goodbye world!".to_string())
            .unwrap();
        assert_eq!(previous, "Hello world!");
        assert_eq!(data.get_string("group.string").unwrap(), "Goodbye world!");
        let result =
            data.set_string("does_not_exist", "Goodbye world!".to_string());
        assert!(result.is_err());
        let result = data.set_string("group.int", "Goodbye world!".to_string());
        assert!(result.is_err());
    }

    #[test]
    fn test_apply() {
        let mut data =
            Data::load(Path::new("resources/tests/test_config_a.json"))
                .unwrap();

        data.apply(
            &Data::load(Path::new("resources/tests/test_config_b.json"))
                .unwrap(),
        );

        assert_eq!(data.get_i64("group.int").unwrap(), 66);
        assert_eq!(data.get_string("group.string").unwrap(), "Hello world!");
        let result = data.get_string("group.group2.string");
        assert!(result.is_none());
        let int_list = data.get_list("int_list").unwrap();
        assert_eq!(int_list.len(), 3);
        assert_eq!(int_list[0], Node::Int(5));
        assert_eq!(int_list[1], Node::Int(6));
        assert_eq!(int_list[2], Node::Int(7));
        let mixed_list = data.get_list("mixed_list").unwrap();
        assert_eq!(mixed_list.len(), 3);
        assert_eq!(mixed_list[0], Node::String("Hello".to_string()));
        assert_eq!(mixed_list[1], Node::Bool(true));
        assert_eq!(mixed_list[2], Node::Float(3.14));
        assert_eq!(data.get_bool("bool").unwrap(), true);
        assert_eq!(data.get_i64("int").unwrap(), 42);
        let result = data.get_string("integer");
        assert!(result.is_none());
        assert_eq!(data.get_f32("float").unwrap(), 3.14);
        assert_eq!(data.get_string("string").unwrap(), "new string");
    }

    #[test]
    fn test_serialize_data() {
        let mut group = HashMap::new();
        group.insert("key".to_string(), Node::Int(14));
        let root = Node::Group(group);
        let data = Data {
            root: RwLock::new(root),
        };

        let serialized = serde_json::to_value(&data).unwrap();
        let expected = serde_json::json!({
            "key": 14
        });

        assert_eq!(serialized, expected);

        let mut sub_group = HashMap::new();
        sub_group.insert("child1".to_string(), Node::Float(3.14));
        sub_group.insert(
            "child2".to_string(),
            Node::List(vec![Node::Bool(true), Node::Bool(false)]),
        );
        let mut group = HashMap::new();
        group.insert(
            "child1".to_string(),
            Node::String("Hello World".to_string()),
        );
        group.insert("child2".to_string(), Node::Group(sub_group));
        let root = Node::Group(group);
        let data = Data {
            root: RwLock::new(root),
        };

        let serialized = serde_json::to_value(&data).unwrap();
        let expected = serde_json::json!({
            "child1": "Hello World",
            "child2": {
                "child1": 3.14,
                "child2": [true, false]
            }
        });
        assert_eq!(serialized, expected);
    }

    #[test]
    fn test_deserialize_data() {
        let json = "{}";
        let data: Data = serde_json::from_str(json).unwrap();
        let root = data.root.read().unwrap();
        match &*root {
            Node::Group(map) => assert!(map.is_empty()),
            _ => panic!("Deserialized data does not contain an empty group"),
        }

        let json = r#"{"a": true, "b": "Hello world!"}"#;
        let data: Data = serde_json::from_str(json).unwrap();
        let root = data.root.read().unwrap();
        match &*root {
            Node::Group(map) => {
                if let Node::Bool(b) = map.get("a").unwrap() {
                    assert_eq!(*b, true);
                } else {
                    panic!("Child \"a\" is not a bool");
                }
                if let Node::String(s) = map.get("b").unwrap() {
                    assert_eq!(*s, "Hello world!".to_string());
                } else {
                    panic!("Child \"b\" is not a string");
                }
            }
            _ => panic!("Deserialized node is not a group"),
        }
    }
}
