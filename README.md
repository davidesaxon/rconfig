# RConfig


## Description
RConfig is a simple hierarchical program configuration using JSON for Rust
projects.

## Usage

Configuration files are intended to be loaded from JSON data. Once loaded the
keys in the config are immutable, but their values can be overloaded at runtime.

Example config JSON data:

```json
{
    "group": {
        "int": 14,
        "string": "Hello world!"
    },
    "int_list": [1, 2, 3, 4],
    "mixed_list": ["Hello", true, 3.14],
    "bool": true,
    "int": 42,
    "float": 3.14,
    "string": "test_test"
}
```

If the above JSON file was stored in a file on disk it could be loaded using:

```rust
let data: Data = Data::load(Path::new("resources/tests/test_config_a.json"))
    .unwrap();
```

Likewise, if the JSON was in an in-memory serde_json value named `json`, it
could be loaded using:

```rust
let data: Data = serde_json::from_value(json).unwrap();
```

Values from the loaded configuration can be accessed as their `Node` enum
objects using the `get_node` function:

```rust
if let Node::Bool(b) = data.get_node("bool").unwrap() {
    assert_eq!(*b, true);
}
```

But for simplicitly, in most cases its easiest to use the typed getter
functions:

```rust
assert_eq!(data.get_bool("bool").unwrap(), true);
assert_eq!(data.get_string("string").unwrap(), "test_test");
```

Children of groups can be access using `.` syntax, for example:

```rust
assert_eq!(data.get_string("group.string").unwrap(), "Hello world!");
```

or:

```rust
if let Node::String(s) = data.get_node("group.string").unwrap() {
    assert_eq!(s, "Hello world!");
}
```

The `get_list` function returns a `Node::List` which contains a Vector of nodes,
so in this case, the underlying `Node` objects must be interacted with:

```rust
let int_list = data.get_list("int_list").unwrap();
assert_eq!(int_list.len(), 4);
assert_eq!(int_list[0], Node::Int(1));
assert_eq!(int_list[1], Node::Int(2));
```

RConfig data keys and types are immutable once created but the values of the
keys can be updated at runtime. For example the value of the existing
"group.string" key can be updated using:

```rust
data.set_string("group.string", "Goodbye world!".to_string());
```

or:

```rust
data.set_node("group.string", Node::String("Goodbye world!".to_string()));
```

One instance of configuration `Data` values can be applied to another
configuration `Data` values using the `apply` function. This is useful for cases
such as loading a default configuration, and then a user override can be applied
by loading seperate JSON. For example:

```rust
let mut data: Data = Data::load(Path::new("resources/tests/test_config_a.json"))
    .unwrap();
let override_json = serde_json::json!({
    {
        "group": "Goodbye world!",
    },
    "int_list": [5, 6, 7],
    "bool": false
});
let override_data: Data = serde_json::from_value(user_override_json).unwrap();
data.apply(&override_data);
```

Note: to apply an override only a subset of the original configuration keys
need to be supplied, the values of keys not specified will be left unchanged.

## License
GNU General Public License v3.0
